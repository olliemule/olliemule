<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
	'*' => array(
			'tablePrefix' => 'craft',
	),
 'live' => array(
			'user' => 'forge',
			'password' => 'C6CSuqQ3QLukFsEGijnv',
			'database' => 'olliemulelive',
			'server' => '104.236.139.209'
	),
	'local' => array(
			'user' => 'homestead',
			'password' => 'secret',
			'database' => 'olliemuledev',
			'server' => 'localhost'
	)
);
