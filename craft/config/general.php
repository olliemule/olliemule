<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
 '*' => array(
	 'enableCsrfProtection' => true,
	 'omitScriptNameInUrls' => true,
	 'pageTrigger' => 'page/',
	 'sendPoweredByHeader' => false,
	 'phpSessionName' => 'sesh',
	 'defaultWeekStartDay' => 0,
	 'cpTrigger' => 'admin'
 ),
 'live' => array(
	 'siteUrl' => 'https://olliemule.com',
	 'devMode' => false,
	 'maxUploadFileSize' => 33554432,
	 'environmentVariables' => array(
		 'assetsBaseUrl' => 'https://olliemule.com/assets',
		 'assetsBasePath' => '/home/forge/olliemule.com/public/assets',
	 ),
 ),
 'local' => array(
	 'siteUrl' => 'http://olliemule.tests',
	 'devMode' => false,
	 'maxUploadFileSize' => 33554432,
	 'environmentVariables' => array(
		 'assetsBaseUrl' => 'http://olliemule.tests/assets',
		 'assetsBasePath' => '../public/assets',
	 ),
 ),
);
