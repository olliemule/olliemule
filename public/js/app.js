$(document).foundation();

$(document).ready(function(){
  $('#menu').slicknav({
    prependTo: 'header'
  });

  if ($(".flipper").length > 0) {
    $("#open-newsletter").click(function(){
      $(".flipper").addClass('flip');
    });

    $("#close-newsletter").click(function(){
      $(".flipper").removeClass('flip');
    });
  }

  $(".owl-carousel").owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots:false,
    navText:[
      '<',
      '>'
    ],
    responsive:{
        0:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:3
        }
    }
  });
});
